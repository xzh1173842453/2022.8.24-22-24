package cn.itsource.user.mapper;

import cn.itsource.user.domain.User;
import cn.itsource.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Tom
 * @since 2022-08-24
 */
public interface UserMapper extends BaseMapper<User> {

}
