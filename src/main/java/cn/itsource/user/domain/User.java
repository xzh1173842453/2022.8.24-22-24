package cn.itsource.user.domain;

import cn.itsource.basic.domain.BaseDomain;

import java.util.Date;

/**
 * 实体类：
 */
public class User extends BaseDomain{

    private String username;
    private String phone;
    private String email;
    /**
     * 盐值
     */
    private String salt;
    /**
     * 密码，md5加密加盐
     */
    private String password;
    /**
     * 员工状态 - 1启用，0禁用
     */
    private Integer state;
    private Integer age;
    private Date createtime;
    private String headImg;
    private Long logininfoId;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public Long getLogininfoId() {
        return logininfoId;
    }

    public void setLogininfoId(Long logininfoId) {
        this.logininfoId = logininfoId;
    }

    @Override
    public String toString() {
        return "User{" +
        ", username=" + username +
        ", phone=" + phone +
        ", email=" + email +
        ", salt=" + salt +
        ", password=" + password +
        ", state=" + state +
        ", age=" + age +
        ", createtime=" + createtime +
        ", headImg=" + headImg +
        ", logininfoId=" + logininfoId +
        "}";
    }
}
