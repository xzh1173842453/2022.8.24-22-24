package cn.itsource.user.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.itsource.basic.domain.BaseDomain;

/**
 * 实体类：
 */
public class Logininfo extends BaseDomain{

    private String username;
    private String phone;
    private String email;
    private String salt;
    private String password;
    /**
     * 类型 - 0管理员，1用户
     */
    private Integer type;
    /**
     * 启用状态：true可用，false禁用
     */
    private Boolean disable;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getDisable() {
        return disable;
    }

    public void setDisable(Boolean disable) {
        this.disable = disable;
    }

    @Override
    public String toString() {
        return "Logininfo{" +
        ", username=" + username +
        ", phone=" + phone +
        ", email=" + email +
        ", salt=" + salt +
        ", password=" + password +
        ", type=" + type +
        ", disable=" + disable +
        "}";
    }
}
