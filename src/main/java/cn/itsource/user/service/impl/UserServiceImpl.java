package cn.itsource.user.service.impl;

import cn.itsource.user.domain.User;
import cn.itsource.user.service.IUserService;
import cn.itsource.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 业务实现类：
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService {

}
