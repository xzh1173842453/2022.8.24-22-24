package cn.itsource.user.service.impl;

import cn.itsource.user.domain.Logininfo;
import cn.itsource.user.service.ILogininfoService;
import cn.itsource.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 业务实现类：
 */
@Service
public class LogininfoServiceImpl extends BaseServiceImpl<Logininfo> implements ILogininfoService {

}
