package cn.itsource.user.service;

import cn.itsource.user.domain.Logininfo;
import cn.itsource.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tom
 * @since 2022-08-24
 */
public interface ILogininfoService extends IBaseService<Logininfo> {

}
