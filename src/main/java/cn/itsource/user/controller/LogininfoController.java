package cn.itsource.user.controller;

import cn.itsource.user.service.ILogininfoService;
import cn.itsource.user.domain.Logininfo;
import cn.itsource.user.query.LogininfoQuery;
import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/logininfo")
public class LogininfoController {

    @Autowired
    public ILogininfoService logininfoService;

    /**
     * 接口：添加或修改
     * @param logininfo  传递的实体
     * @return AjaxResult 响应给前端
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Logininfo logininfo){
        try {
            if( logininfo.getId()!=null)
                logininfoService.update(logininfo);
            else
                logininfoService.save(logininfo);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"操作失败");
        }
    }

    /**
    * 接口：删除
    * @param id
    * @return AjaxResult 响应给前端
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            logininfoService.remove(id);
            return new AjaxResult();
        } catch (Exception e) {
        e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }

    /**
    * 接口：批量删除
    * @param ids
    * @return AjaxResult 响应给前端
    */
    @PatchMapping
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        try {
                logininfoService.patchRemove(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"批量删除失败");
        }
    }

    /**
    * 接口：查询单个对象
    * @param id
    */
    @GetMapping("/{id}")
    public Logininfo findOne(@PathVariable("id")Long id) {
        return logininfoService.loadById(id);
    }


    /**
    * 接口：查询所有
    * @return
    */
    @GetMapping
    public List<Logininfo> findAll(){
        return logininfoService.loadAll();
    }


   /**
    * 接口：分页查询或高级查询
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public PageList<Logininfo> queryPage(@RequestBody LogininfoQuery query) {
        return logininfoService.queryPage(query);
    }
}
