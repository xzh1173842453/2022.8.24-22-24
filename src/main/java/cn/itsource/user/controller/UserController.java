package cn.itsource.user.controller;

import cn.itsource.user.service.IUserService;
import cn.itsource.user.domain.User;
import cn.itsource.user.query.UserQuery;
import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    public IUserService userService;

    /**
     * 接口：添加或修改
     * @param user  传递的实体
     * @return AjaxResult 响应给前端
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody User user){
        try {
            if( user.getId()!=null)
                userService.update(user);
            else
                userService.save(user);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"操作失败");
        }
    }

    /**
    * 接口：删除
    * @param id
    * @return AjaxResult 响应给前端
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            userService.remove(id);
            return new AjaxResult();
        } catch (Exception e) {
        e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }

    /**
    * 接口：批量删除
    * @param ids
    * @return AjaxResult 响应给前端
    */
    @PatchMapping
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        try {
                userService.patchRemove(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"批量删除失败");
        }
    }

    /**
    * 接口：查询单个对象
    * @param id
    */
    @GetMapping("/{id}")
    public User findOne(@PathVariable("id")Long id) {
        return userService.loadById(id);
    }


    /**
    * 接口：查询所有
    * @return
    */
    @GetMapping
    public List<User> findAll(){
        return userService.loadAll();
    }


   /**
    * 接口：分页查询或高级查询
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public PageList<User> queryPage(@RequestBody UserQuery query) {
        return userService.queryPage(query);
    }
}
