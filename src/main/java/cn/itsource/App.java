package cn.itsource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: 启动类
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:22
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
//@EnableAutoConfiguration - 自动配置（开启Spring对mvc的注解支持，静态资源放行）
//@ComponentScan - 扫描当前包及其子包
@SpringBootApplication
//启动时候扫描cn.itsource.*.mapper包写所有接口 - 自动生成该接口的实现类并创建代理对象
@MapperScan("cn.itsource.*.mapper")
public class App {

    public static void main(String[] args) {
        //使用SpringBoot项目的启动类 去运行 Spring项目
        SpringApplication.run(App.class,args);
    }
}
//申明当前类是SpringBoot项目的启动类
