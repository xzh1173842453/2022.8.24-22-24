package cn.itsource.basic.service;

/**
 * @description:
 * @author: Bug修复机
 * @date: 2022年08月24日 ： 16:44
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public interface IVerifyCodeService {
    //生成图形验证码
    String image(String key);
}
