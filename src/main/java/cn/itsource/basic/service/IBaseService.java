package cn.itsource.basic.service;

import cn.itsource.basic.query.BaseQuery;
import cn.itsource.basic.util.PageList;
import java.util.List;

/**
 * @description: 业务接口：部门
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:21
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public interface IBaseService<T> {

    /**
     * 保存对象
     * @param t
     */
    void save(T t);

    /**
     * 移除对象
     * @param id
     */
    void remove(Long id);

    /**
     * 修改对象
     * @param t
     */
    void update(T t);

    /**
     * 加载单个对象
     * @param id
     * @return
     */
    T loadById(Long id);

    /**
     * 加载所有
     * @return
     */
    List<T> loadAll();

    /**
     * 分页
     * @param baseQuery
     * @return
     */
    PageList<T> queryPage(BaseQuery baseQuery);

    void patchRemove(List<Long> ids);
}
