package cn.itsource.basic.service.impl;

import cn.itsource.basic.service.IVerifyCodeService;
import cn.itsource.basic.util.StrUtils;
import cn.itsource.basic.util.VerifyCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: Bug修复机
 * @date: 2022年08月24日 ： 16:45
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Service
public class VerifyCodeServiceImpl implements IVerifyCodeService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public String image(String key) {
        //使用工具类生成图形验证码
        String code = StrUtils.getComplexRandomString(4);//1213
        //保存到redis
        redisTemplate.opsForValue().set(key,code,5, TimeUnit.MINUTES);
        //使用工具类生成图形验证码编码之后的base64的字符串
        String base64Str = VerifyCodeUtils.verifyCode(110, 40, code);
        return base64Str;
    }
}
