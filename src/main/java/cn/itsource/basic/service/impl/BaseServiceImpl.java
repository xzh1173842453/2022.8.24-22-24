package cn.itsource.basic.service.impl;

import cn.itsource.basic.mapper.BaseMapper;
import cn.itsource.basic.query.BaseQuery;
import cn.itsource.basic.service.IBaseService;
import cn.itsource.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @description: 业务实现类：部门
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:21
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class BaseServiceImpl<T> implements IBaseService<T> {

    //注入：从Spring容器中获取当前类或子类的对象  赋值 给当前属性
    @Autowired
    //IDEA以为Spring容器中没有该类型或其子类型的对象，又在使用，所以报错
    //它错了 - 因为已经通过在App中@MapperScan("cn.itsource.*.mapper")已经生成了
    private BaseMapper<T> baseMapper;

    @Override
    @Transactional //管理事务的注解，默认readOnly = false,propagation = Propagation.REQUIRED
    public void save(T t) {
        baseMapper.save(t);
    }

    @Override
    @Transactional
    public void remove(Long id) {
        baseMapper.remove(id);
    }

    @Override
    @Transactional
    public void update(T t) {
        baseMapper.update(t);
    }

    @Override
    public T loadById(Long id) {
        return baseMapper.loadById(id);
    }

    @Override
    public List<T> loadAll() {
        return baseMapper.loadAll();
    }

    @Override
    public PageList<T> queryPage(BaseQuery baseQuery) {
        //1.查询总条数
        Integer totals = baseMapper.queryCount(baseQuery);
        //如果总数为0，没有必要去查询分页数据了 - 反正查询出来都是一个空集合
        if(totals==0){
            return new PageList<>();
        }

        //2.查询当前页的分页数据
        List<T> data = baseMapper.queryData(baseQuery);
        return new PageList<>(data,totals);
    }

    @Override
    public void patchRemove(List<Long> ids) {
        baseMapper.patchRemove(ids);
    }
}
