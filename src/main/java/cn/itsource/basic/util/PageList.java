package cn.itsource.basic.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


/**
 * @description: 工具类：封装分页或高级查询的返回数据
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:19
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Data
@AllArgsConstructor //全参
@NoArgsConstructor //无参
public class PageList<T>  {
    //当前页的分页数据
    private List<T> data = new ArrayList<>();
    //总数量
    private Integer totals = 0;
}
