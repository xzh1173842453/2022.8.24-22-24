package cn.itsource.basic.util;

/**
 * @description: 工具类：封装增删改的返回数据
 * @author: Bug修复机
 * @date: 2022年08月14日 ： 10:14
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public class AjaxResult {
    private Boolean success = true;
    private String msg = "操作成功";
    //保存其他信息
    private Object resultObj;

    //获取当前对象
    public static AjaxResult me(){
        return new AjaxResult();
    }

    public AjaxResult() {
    }
    public AjaxResult(Boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public Boolean getSuccess() {
        return success;
    }


    public AjaxResult setSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public AjaxResult setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getResultObj() {
        return resultObj;
    }

    public AjaxResult setResultObj(Object resultObj) {
        this.resultObj = resultObj;
        return this;
    }



    @Override
    public String toString() {
        return "AjaxResult{" +
                "success=" + success +
                ", msg='" + msg + '\'' +
                ", resultObj=" + resultObj +
                '}';
    }

    public static void main(String[] args) {
        System.out.println(AjaxResult.me().setSuccess(false).setMsg("呵呵").setResultObj("哈哈"));
    }
}
