package cn.itsource.basic.exception;

/**
 * @description: 业务异常
 * @author: Bug修复机
 * @date: 2022年08月21日 ： 14:20
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public class BusinessException extends RuntimeException{
    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }
}
