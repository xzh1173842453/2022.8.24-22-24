package cn.itsource.basic.domain;

import lombok.Data;

/**
 * @description: 公共的实体类
 * @author: Bug修复机
 * @date: 2022年08月17日 ： 10:35
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Data
public class BaseDomain {
    //ID
    private Long id;
}
