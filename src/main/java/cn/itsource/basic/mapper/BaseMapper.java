package cn.itsource.basic.mapper;


import cn.itsource.basic.query.BaseQuery;

import java.util.List;

/**
 * @description: Mapper接口：公共的
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:20
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public interface BaseMapper<T> {

    /**
     * 保存对象
     * @param t
     */
    void save(T t);

    /**
     * 移除对象
     * @param id
     */
    void remove(Long id);

    /**
     * 修改对象
     * @param t
     */
    void update(T t);

    /**
     * 加载单个对象
     * @param id
     * @return
     */
    T loadById(Long id);

    /**
     * 加载所有
     * @return
     */
    List<T> loadAll();

    /**
     * @description: 查询总数量
     * @author: Bug修复机
     * @date: 2022/8/17 : 10:39
     * @param:
     * @return:
     */
    Integer queryCount(BaseQuery baseQuery);

    /**
     * @description: 查询当前页的分页数据
     * @author: Bug修复机
     * @date: 2022/8/17 : 10:39
     * @param:
     * @return:
     */
    List<T> queryData(BaseQuery baseQuery);

    /**
     * @description: 批量删除
     * @author: Bug修复机
     * @date: 2022/8/17 : 10:39
     * @param:
     * @return:
     */
    void patchRemove(List<Long> ids);
}
