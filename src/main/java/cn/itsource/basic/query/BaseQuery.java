package cn.itsource.basic.query;

import lombok.Data;

/**
 * @description: 公共查询类
 * @author: 源码时代 - Tom老师
 * @date: 2022/8/12 : 18:58
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Data
public class BaseQuery {
    //当前页
    private Integer currentPage = 1;

    //每页显示的条数
    private Integer pageSize = 5;

    //关键字查询
    private String keyword;

    
    /**
     * @description: 计算limit的第一个参数：limit a,b a-当前页的起始位置，b=pageSize
     * @author: 汤神
     * @date: 2022/8/12 : 19:06
     * @param:
     * @return: 
     */
    public Integer getBegin() {
        return (this.currentPage-1)*this.pageSize ;
    }
}
