package cn.itsource.basic.controller;

import cn.itsource.basic.service.IVerifyCodeService;
import cn.itsource.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 验证码接口类
 * @author: Bug修复机
 * @date: 2022年08月24日 ： 16:40
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@RestController
@RequestMapping("/verifyCode")
public class VerifyCodeController {

    @Autowired
    private IVerifyCodeService verifyCodeService;

    @GetMapping("/image/{key}")
    public AjaxResult image(@PathVariable("key")String key){
        try {
            //调用业务代码获取图形验证码（base64编码的字符串）
            String base64Str = verifyCodeService.image(key);
            //将（base64编码的字符串）响应给前端
            return AjaxResult.me().setResultObj(base64Str);
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"获取图形验证码失败");
        }
    }
}
