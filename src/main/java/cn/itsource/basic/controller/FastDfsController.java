package cn.itsource.basic.controller;

import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.FastDfsUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description: 公共接口：fastDfs
 * @author: Bug修复机
 * @date: 2022年08月21日 ： 17:08
 * @version: ver1.0
 * @email tangli@itsource.cn
 * 上传：写
 * 删除：写
 * 修改：不写 = 先删再上传
 * 查询：不写 通过http访问即可
 */
@RestController
@RequestMapping("/fastDfs")
public class FastDfsController {

    /**
     * 上传接口
     * @param file
     * @return
     * @RequestPart - 专门用来接收二进制文件
     * MultipartFile file - 于eleme提供的上传组件中name="file"一致
     */
    @PostMapping
    public AjaxResult upload(@RequestPart MultipartFile file){
        try {
            //获取原始文件名： 11.404.png
            String oName = file.getOriginalFilename();
            String[] arr = oName.split("\\.");
            //  /group1/M00/00/09/rBEACmKXF8-AUc6KAANsldwx3H4713.jpg
            String fileName = FastDfsUtils.upload(file.getBytes(), arr[arr.length - 1]);
            return AjaxResult.me().setResultObj(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"上传失败");
        }
    }

    /**
     * 删除接口
     * @param path
     * @return
     */
    //  http://localhost:8080/fastDfs//group1/M00/00/09/rBEACmKXF8-AUc6KAANsldwx3H4713.jpg   - Delete - 很麻烦
    //  http://localhost:8080/fastDfs?path=/group1/M00/00/09/rBEACmKXF8-AUc6KAANsldwx3H4713.jpg   - Delete
    @DeleteMapping
    public AjaxResult delete(@RequestParam("path") String path){
        // /group1     /M00/00/09/rBEACmKXF8-AUc6KAANsldwx3H4713.jpg
        try {
            String path1 = path.substring(1); //  group1/M00/00/09/rBEACmKXF8-AUc6KAANsldwx3H4713.jpg
            String para1 = path1.substring(0,path1.indexOf("/")); //group1
            String para2 = path1.substring(path1.indexOf("/")+1); //   M00/00/09/rBEACmKXF8-AUc6KAANsldwx3H4713.jpg
            FastDfsUtils.delete(para1,para2);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }


}
