package cn.itsource.system.domain;

import cn.itsource.basic.domain.BaseDomain;
import lombok.Data;

/**
 * @function: 实体类：数据字典明细
 * @author: Tom
 * @date: 2022年08月18日 ： 20:30
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Data
public class SystemDictionaryDetail extends BaseDomain {
    //数据字典明细名称
    private String name;
    //数据字典明细所属类型id
    private Long types_id;
    //关联类型对象
    private SystemDictionaryType type;
}
