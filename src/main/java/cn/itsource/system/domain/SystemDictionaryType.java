package cn.itsource.system.domain;

import cn.itsource.basic.domain.BaseDomain;
import lombok.Data;

/**
 * @function: 实体类：数据字典类型
 * @author: Tom
 * @date: 2022年08月18日 ： 20:30
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Data
public class SystemDictionaryType extends BaseDomain {
    //数据字典类型编号
    private String sn;
    //数据字典类型名称
    private String name;
}
