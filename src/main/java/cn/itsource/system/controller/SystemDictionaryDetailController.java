package cn.itsource.system.controller;

import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.PageList;
import cn.itsource.system.domain.SystemDictionaryDetail;
import cn.itsource.system.query.SystemDictionaryDetailQuery;
import cn.itsource.system.service.ISystemDictionaryDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 数据字典明细接口类
 * @author: Bug修复机
 * @date: 2022年08月14日 ： 9:47
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Api(value = "数据字典明细接口类",description = "数据字典明细接口类")
@RestController //@Controller + @ResponseBody
@RequestMapping("/systemDictionaryDetail")
public class SystemDictionaryDetailController {

    @Autowired
    private ISystemDictionaryDetailService systemDictionaryDetailService;


    /**
     * @description: 查询单个
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080/systemDictionaryDetail/1 Get
    @ApiOperation(value = "查询单个")
    @GetMapping("/{id}")
    public SystemDictionaryDetail findOne(@PathVariable("id")Long id){
        return systemDictionaryDetailService.loadById(id);
    }


    /**
     * @description: 查询所有
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080/systemDictionaryDetail Get
    @ApiOperation(value = "查询所有")
    @GetMapping
    public List<SystemDictionaryDetail> findAll(){
        return systemDictionaryDetailService.loadAll();
    }


    /**
     * @description: 删除接口
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:26
     * @param:
     * @return:
     */
    // http://localhost:8080/systemDictionaryDetail/1 Delete
    @ApiOperation(value = "删除接口")
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id")Long id){
        try {
            systemDictionaryDetailService.remove(id);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }

    /**
     * @description: 添加或修改
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:26
     * @param:
     * @return:
     */
    // http://localhost:8080/systemDictionaryDetail Put 参数：{} - 请求体
    @ApiOperation(value = "添加或修改")
    @PutMapping
    //@RequestBody SystemDictionaryDetail systemDictionaryDetail - 从请求体中获取数据赋值给SystemDictionaryDetail对象
    public AjaxResult addOrUpdate(@RequestBody SystemDictionaryDetail systemDictionaryDetail){
        try {
            if(systemDictionaryDetail.getId()==null){//添加
                systemDictionaryDetailService.save(systemDictionaryDetail);
            }else{
                systemDictionaryDetailService.update(systemDictionaryDetail);
            }
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"操作失败");
        }
    }


    /**
     * @description: 分页接口
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:31
     * @param:
     * @return:
     */
    // http://localhost:8080/ PosystemDictionaryDetailst 参数：{} - 请求体
    @ApiOperation(value = "分页接口")
    @PostMapping
    public PageList<SystemDictionaryDetail> queryPage(@RequestBody SystemDictionaryDetailQuery systemDictionaryDetailQuery){
        return systemDictionaryDetailService.queryPage(systemDictionaryDetailQuery);
    }


    /**
     * @description: 批量删除接口
     * @author: Tom
     * @date: 2022年08月18日 : 22:52
     * @param:
     * @return:
     */
    @ApiOperation(value = "批量删除接口")
    @PatchMapping
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        try {
            systemDictionaryDetailService.patchRemove(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"批量删除失败");
        }
    }


    /**
     * @description: 根据类型id查询当前类型所有明细
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080//systemDictionaryDetail/getByTypeId/1 Get
    @ApiOperation(value = "根据类型id查询当前类型所有明细")
    @GetMapping("/getByTypeId/{id}")
    public List<SystemDictionaryDetail> getDetailByTypeId(@PathVariable("id")Long id){
        return systemDictionaryDetailService.getDetailByTypeId(id);
    }

}
