package cn.itsource.system.controller;

import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.PageList;
import cn.itsource.system.domain.SystemDictionaryType;
import cn.itsource.system.query.SystemDictionaryTypeQuery;
import cn.itsource.system.service.ISystemDictionaryTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 数据字典类型接口类
 * @author: Bug修复机
 * @date: 2022年08月14日 ： 9:47
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Api(value = "数据字典类型接口类",description = "数据字典类型接口类")
@RestController //@Controller + @ResponseBody
@RequestMapping("/systemDictionaryType")
public class SystemDictionaryTypeController {

    @Autowired
    private ISystemDictionaryTypeService systemDictionaryTypeService;


    /**
     * @description: 查询单个
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080/systemDictionaryType/1 Get
    @ApiOperation(value = "查询单个")
    @GetMapping("/{id}")
    public SystemDictionaryType findOne(@PathVariable("id")Long id){
        return systemDictionaryTypeService.loadById(id);
    }


    /**
     * @description: 查询所有
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080/systemDictionaryType Get
    @ApiOperation(value = "查询所有")
    @GetMapping
    public List<SystemDictionaryType> findAll(){
        return systemDictionaryTypeService.loadAll();
    }


    /**
     * @description: 删除接口
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:26
     * @param:
     * @return:
     */
    // http://localhost:8080/systemDictionaryType/1 Delete
    @ApiOperation(value = "删除接口")
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id")Long id){
        try {
            systemDictionaryTypeService.remove(id);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }

    /**
     * @description: 添加或修改
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:26
     * @param:
     * @return:
     */
    // http://localhost:8080/systemDictionaryType Put 参数：{} - 请求体
    @ApiOperation(value = "添加或修改")
    @PutMapping
    //@RequestBody SystemDictionaryType systemDictionaryType - 从请求体中获取数据赋值给SystemDictionaryType对象
    public AjaxResult addOrUpdate(@RequestBody SystemDictionaryType systemDictionaryType){
        try {
            if(systemDictionaryType.getId()==null){//添加
                systemDictionaryTypeService.save(systemDictionaryType);
            }else{
                systemDictionaryTypeService.update(systemDictionaryType);
            }
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"操作失败");
        }
    }


    /**
     * @description: 分页接口
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:31
     * @param:
     * @return:
     */
    // http://localhost:8080/ PosystemDictionaryTypest 参数：{} - 请求体
    @ApiOperation(value = "分页接口")
    @PostMapping
    public PageList<SystemDictionaryType> queryPage(@RequestBody SystemDictionaryTypeQuery systemDictionaryTypeQuery){
        return systemDictionaryTypeService.queryPage(systemDictionaryTypeQuery);
    }


    @ApiOperation(value = "批量删除接口")
    @PatchMapping
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        try {
            systemDictionaryTypeService.patchRemove(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"批量删除失败");
        }
    }

}
