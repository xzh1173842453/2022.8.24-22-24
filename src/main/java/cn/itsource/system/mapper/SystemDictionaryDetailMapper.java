package cn.itsource.system.mapper;

import cn.itsource.basic.mapper.BaseMapper;
import cn.itsource.system.domain.SystemDictionaryDetail;

import java.util.List;

/**
 * @description: Mapper接口：数据字典明细
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:20
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public interface SystemDictionaryDetailMapper extends BaseMapper<SystemDictionaryDetail> {

    //根据类型id删除数据字典明细
    void removeByTypeId(Long id);
    //根据类型id批量删除数据字典明细
    void patchRemoveByTypeId(List<Long> ids);
    //根据类型id查询当前类型所有明细
    List<SystemDictionaryDetail> getDetailByTypeId(Long id);
}
