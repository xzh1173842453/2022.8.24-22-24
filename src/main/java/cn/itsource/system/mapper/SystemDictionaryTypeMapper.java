package cn.itsource.system.mapper;

import cn.itsource.basic.mapper.BaseMapper;
import cn.itsource.system.domain.SystemDictionaryType;

/**
 * @description: Mapper接口：数据字典类型
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:20
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public interface SystemDictionaryTypeMapper extends BaseMapper<SystemDictionaryType> {

}
