package cn.itsource.system.service;

import cn.itsource.basic.service.IBaseService;
import cn.itsource.system.domain.SystemDictionaryType;

/**
 * @description: 业务接口：数据字典类型
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:21
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public interface ISystemDictionaryTypeService extends IBaseService<SystemDictionaryType> {

}
