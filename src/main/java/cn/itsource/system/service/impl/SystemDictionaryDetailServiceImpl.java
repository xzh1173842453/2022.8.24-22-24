package cn.itsource.system.service.impl;

import cn.itsource.basic.service.impl.BaseServiceImpl;
import cn.itsource.system.domain.SystemDictionaryDetail;
import cn.itsource.system.mapper.SystemDictionaryDetailMapper;
import cn.itsource.system.service.ISystemDictionaryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: 业务实现类：数据字典明细
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:21
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Service //1.将当前类交给Spring管理 2.实例化对象的注解 //前提：扫描包路径
public class SystemDictionaryDetailServiceImpl extends BaseServiceImpl<SystemDictionaryDetail> implements ISystemDictionaryDetailService {

    @Autowired
    private SystemDictionaryDetailMapper systemDictionaryDetailMapper;

    @Override
    public List<SystemDictionaryDetail> getDetailByTypeId(Long id) {
        return systemDictionaryDetailMapper.getDetailByTypeId(id);
    }
}
