package cn.itsource.system.service;

import cn.itsource.basic.service.IBaseService;
import cn.itsource.system.domain.SystemDictionaryDetail;

import java.util.List;

/**
 * @description: 业务接口：数据字典明细
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:21
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public interface ISystemDictionaryDetailService extends IBaseService<SystemDictionaryDetail> {

    //根据类型id查询当前类型所有明细
    List<SystemDictionaryDetail> getDetailByTypeId(Long id);
}
