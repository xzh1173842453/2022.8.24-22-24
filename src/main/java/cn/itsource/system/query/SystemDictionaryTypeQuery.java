package cn.itsource.system.query;

import cn.itsource.basic.query.BaseQuery;

/**
 * @description: 查询类：数据字典类型
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:21
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public class SystemDictionaryTypeQuery extends BaseQuery {
}
