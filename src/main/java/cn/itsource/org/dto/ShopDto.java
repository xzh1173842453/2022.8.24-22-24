package cn.itsource.org.dto;

import lombok.Data;

/**
 * @description: 数据传输对象
 * @author: Bug修复机
 * @date: 2022年08月22日 ： 19:22
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Data
public class ShopDto {
    //状态
    private Integer state;
    //当前状态的数量
    private Integer num;
}
