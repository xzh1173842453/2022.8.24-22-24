package cn.itsource.org.controller;

import cn.itsource.basic.exception.BusinessException;
import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.ExcelUtils;
import cn.itsource.basic.util.PageList;
import cn.itsource.org.domain.Shop;
import cn.itsource.org.domain.ShopAuditLog;
import cn.itsource.org.dto.ShopDto;
import cn.itsource.org.query.ShopQuery;
import cn.itsource.org.service.IShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/shop")
public class ShopController {

    @Autowired
    public IShopService shopService;

    /**
     * 接口：添加或修改
     * @param shop  传递的实体
     * @return AjaxResult 响应给前端
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Shop shop){
        try {
            if( shop.getId()!=null)
                shopService.update(shop);
            else
                shopService.save(shop);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"操作失败");
        }
    }

    /**
    * 接口：删除
    * @param id
    * @return AjaxResult 响应给前端
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            shopService.remove(id);
            return new AjaxResult();
        } catch (Exception e) {
        e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }

    /**
    * 接口：批量删除
    * @param ids
    * @return AjaxResult 响应给前端
    */
    @PatchMapping
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        try {
                shopService.patchRemove(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"批量删除失败");
        }
    }

    /**
    * 接口：查询单个对象
    * @param id
    */
    @GetMapping("/{id}")
    public Shop findOne(@PathVariable("id")Long id) {
        return shopService.loadById(id);
    }


    /**
    * 接口：查询所有
    * @return
    */
    @GetMapping
    public List<Shop> findAll(){
        return shopService.loadAll();
    }


   /**
    * 接口：分页查询或高级查询
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public PageList<Shop> queryPage(@RequestBody ShopQuery query) {
        return shopService.queryPage(query);
    }



    @PostMapping(value="/settlement")
    public AjaxResult settlement(@RequestBody Shop shop){
        try {
            shopService.settlement(shop);
            return new AjaxResult();
        } catch (BusinessException e) { //捕捉自己抛出的异常
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        } catch (Exception e) { //500
            e.printStackTrace();
            return new AjaxResult(false,"系统繁忙，请稍后重试!!!");
        }
    }



    @PostMapping("/audit/pass")
    public AjaxResult auditPass(@RequestBody ShopAuditLog shopAuditLog){
        try {
            shopService.auditPass(shopAuditLog);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"系统异常，请稍后重试");
        }
    }

    @PostMapping("/audit/reject")
    public AjaxResult auditReject(@RequestBody ShopAuditLog shopAuditLog){
        try {
            shopService.auditReject(shopAuditLog);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"系统异常，请稍后重试");
        }
    }


    @GetMapping("/export")
    public void export(HttpServletResponse response){
        List<Shop> shops = shopService.loadAll();
        //List<?> list, String title, String sheetName, Class<?> pojoClass,String fileName,
        // HttpServletResponse response
        ExcelUtils.exportExcel(shops,null,"店铺数据",Shop.class,"系统数据.xlsx",response);
    }

    @PostMapping("/importExcel")
    public void importExcel(@RequestPart MultipartFile file){
        //参数说明：参数1是前端传递的复杂文件。参数2是标题的行数，参数3是表头的行数，参数4是类型
        List<Shop> shops = ExcelUtils.importExcel(file, 0, 1, Shop.class);
        //后续可以通过批量添加：forEach 添加到数据库
        shops.forEach(System.out::println);
    }


    @GetMapping("/echarts")
    public List<ShopDto> echarts(){
        return shopService.echarts();
    }

}
