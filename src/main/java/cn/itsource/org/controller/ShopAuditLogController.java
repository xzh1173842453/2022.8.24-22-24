package cn.itsource.org.controller;

import cn.itsource.org.service.IShopAuditLogService;
import cn.itsource.org.domain.ShopAuditLog;
import cn.itsource.org.query.ShopAuditLogQuery;
import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
* 后端接口类；
*/
@RestController
@RequestMapping("/shopAuditLog")
public class ShopAuditLogController {

    @Autowired
    public IShopAuditLogService shopAuditLogService;

    /**
     * 接口：添加或修改
     * @param shopAuditLog  传递的实体
     * @return AjaxResult 响应给前端
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody ShopAuditLog shopAuditLog){
        try {
            if( shopAuditLog.getId()!=null)
                shopAuditLogService.update(shopAuditLog);
            else
                shopAuditLogService.save(shopAuditLog);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"操作失败");
        }
    }

    /**
    * 接口：删除
    * @param id
    * @return AjaxResult 响应给前端
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            shopAuditLogService.remove(id);
            return new AjaxResult();
        } catch (Exception e) {
        e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }

    /**
    * 接口：批量删除
    * @param ids
    * @return AjaxResult 响应给前端
    */
    @PatchMapping
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        try {
                shopAuditLogService.patchRemove(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"批量删除失败");
        }
    }

    /**
    * 接口：查询单个对象
    * @param id
    */
    @GetMapping("/{id}")
    public ShopAuditLog findOne(@PathVariable("id")Long id) {
        return shopAuditLogService.loadById(id);
    }


    /**
    * 接口：查询所有
    * @return
    */
    @GetMapping
    public List<ShopAuditLog> findAll(){
        return shopAuditLogService.loadAll();
    }


   /**
    * 接口：分页查询或高级查询
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public PageList<ShopAuditLog> queryPage(@RequestBody ShopAuditLogQuery query) {
        return shopAuditLogService.queryPage(query);
    }
}
