package cn.itsource.org.controller;

import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.PageList;
import cn.itsource.org.domain.Department;
import cn.itsource.org.query.DepartmentQuery;
import cn.itsource.org.service.IDepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 部门接口类
 * @author: Bug修复机
 * @date: 2022年08月14日 ： 9:47
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Api(value = "部门接口类",description = "部门接口类")
@RestController //@Controller + @ResponseBody
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;


    /**
     * @description: 查询单个
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080/department/1 Get
    @ApiOperation(value = "查询单个")
    @GetMapping("/{id}")
    public Department findOne(@PathVariable("id")Long id){
        return departmentService.loadById(id);
    }


    /**
     * @description: 查询所有
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080/department Get
    @ApiOperation(value = "查询所有")
    @GetMapping
    public List<Department> findAll(){
        return departmentService.loadAll();
    }


    /**
     * @description: 删除接口
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:26
     * @param:
     * @return:
     */
    // http://localhost:8080/department/1 Delete
    @ApiOperation(value = "删除接口")
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id")Long id){
        try {
            departmentService.remove(id);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }

    /**
     * @description: 添加或修改
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:26
     * @param:
     * @return:
     */
    // http://localhost:8080/department Put 参数：{} - 请求体
    @ApiOperation(value = "添加或修改")
    @PutMapping
    //@RequestBody Department department - 从请求体中获取数据赋值给Department对象
    public AjaxResult addOrUpdate(@RequestBody Department department){
        try {
            if(department.getId()==null){//添加
                departmentService.save(department);
            }else{
                departmentService.update(department);
            }
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"操作失败");
        }
    }


    /**
     * @description: 分页接口
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:31
     * @param:
     * @return:
     */
    // http://localhost:8080/ Podepartmentst 参数：{} - 请求体
    @ApiOperation(value = "分页接口")
    @PostMapping
    public PageList<Department> queryPage(@RequestBody DepartmentQuery departmentQuery){
        return departmentService.queryPage(departmentQuery);
    }


    @ApiOperation(value = "批量删除接口")
    @PatchMapping
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        try {
            departmentService.patchRemove(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"批量删除失败");
        }
    }


    @ApiOperation(value = "查询部门树")
    @GetMapping("/deptTree")
    public List<Department> deptTree(){
        return departmentService.deptTree();
    }

}
