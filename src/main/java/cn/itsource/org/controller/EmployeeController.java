package cn.itsource.org.controller;

import cn.itsource.basic.util.AjaxResult;
import cn.itsource.basic.util.PageList;
import cn.itsource.org.domain.Employee;
import cn.itsource.org.query.EmployeeQuery;
import cn.itsource.org.service.IEmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 部门接口类
 * @author: Bug修复机
 * @date: 2022年08月14日 ： 9:47
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Api(value = "部门接口类",description = "部门接口类")
@RestController //@Controller + @ResponseBody
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;


    /**
     * @description: 查询单个
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080/employee/1 Get
    @ApiOperation(value = "查询单个")
    @GetMapping("/{id}")
    public Employee findOne(@PathVariable("id")Long id){
        return employeeService.loadById(id);
    }


    /**
     * @description: 查询所有
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:12
     * @param:
     * @return:
     */
    // http://localhost:8080/employee Get
    @ApiOperation(value = "查询所有")
    @GetMapping
    public List<Employee> findAll(){
        return employeeService.loadAll();
    }


    /**
     * @description: 删除接口
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:26
     * @param:
     * @return:
     */
    // http://localhost:8080/employee/1 Delete
    @ApiOperation(value = "删除接口")
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id")Long id){
        try {
            employeeService.remove(id);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"删除失败");
        }
    }

    /**
     * @description: 添加或修改
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:26
     * @param:
     * @return:
     */
    // http://localhost:8080/employee Put 参数：{} - 请求体
    @ApiOperation(value = "添加或修改")
    @PutMapping
    //@RequestBody Employee employee - 从请求体中获取数据赋值给Employee对象
    public AjaxResult addOrUpdate(@RequestBody Employee employee){
        try {
            if(employee.getId()==null){//添加
                employeeService.save(employee);
            }else{
                employeeService.update(employee);
            }
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"操作失败");
        }
    }


    /**
     * @description: 分页接口
     * @author: Bug修复机
     * @date: 2022/8/14 : 10:31
     * @param:
     * @return:
     */
    // http://localhost:8080/ Poemployeest 参数：{} - 请求体
    @ApiOperation(value = "分页接口")
    @PostMapping
    public PageList<Employee> queryPage(@RequestBody EmployeeQuery employeeQuery){
        return employeeService.queryPage(employeeQuery);
    }


    @ApiOperation(value = "批量删除接口")
    @PatchMapping
    public AjaxResult patchRemove(@RequestBody List<Long> ids){
        try {
            employeeService.patchRemove(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,"批量删除失败");
        }
    }

}
