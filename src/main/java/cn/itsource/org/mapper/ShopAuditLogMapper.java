package cn.itsource.org.mapper;

import cn.itsource.org.domain.ShopAuditLog;
import cn.itsource.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Tom
 * @since 2022-08-22
 */
public interface ShopAuditLogMapper extends BaseMapper<ShopAuditLog> {

}
