package cn.itsource.org.mapper;

import cn.itsource.basic.mapper.BaseMapper;
import cn.itsource.org.domain.Employee;
import cn.itsource.org.query.EmployeeQuery;

import java.util.List;

/**
 * @description: Mapper接口：部门
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:20
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}
