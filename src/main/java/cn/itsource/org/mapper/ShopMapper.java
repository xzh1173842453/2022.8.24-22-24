package cn.itsource.org.mapper;

import cn.itsource.org.domain.Shop;
import cn.itsource.basic.mapper.BaseMapper;
import cn.itsource.org.dto.ShopDto;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Tom
 * @since 2022-08-21
 */
public interface ShopMapper extends BaseMapper<Shop> {
    //根据店铺名称和地址判断该店铺是否入驻过
    Shop loadByNameAndAddress(Shop shop);

    List<ShopDto> echarts();
}
