package cn.itsource.org.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.itsource.basic.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 实体类：
 */
public class Shop extends BaseDomain{

    /**
     * 店铺名称
     */
    @Excel(name = "店铺名称",width = 20)
    private String name;
    /**
     * 店铺座机
     */
    @Excel(name = "店铺电话",width = 20)
    private String tel;
    /**
     * 入驻时间
     */
    @Excel(name = "入驻时间",width = 20,exportFormat="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date registerTime = new Date();
    /**
     * 店铺状态：1待审核，2审核通过待激活，3激活成功，4驳回
     */
    @Excel(name = "店铺状态",width = 20)
    private Integer state = 1;
    /**
     * 店铺地址
     */
    @Excel(name = "店铺地址",width = 20)
    private String address;
    /**
     * 店铺logo
     */
    private String logo;
    /**
     * 店铺管理员ID
     */
    private Long adminId;
    //店铺管理员对象
    private Employee admin;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Employee getAdmin() {
        return admin;
    }

    public void setAdmin(Employee admin) {
        this.admin = admin;
    }

    @Override
    public String toString() {
        return "Shop{" +
        ", name=" + name +
        ", tel=" + tel +
        ", registerTime=" + registerTime +
        ", state=" + state +
        ", address=" + address +
        ", logo=" + logo +
        ", adminId=" + adminId +
        "}";
    }
}
