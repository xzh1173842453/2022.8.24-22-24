package cn.itsource.org.domain;

import cn.itsource.basic.domain.BaseDomain;

import java.util.Date;

/**
 * 实体类：
 */
public class ShopAuditLog extends BaseDomain{

    private Integer state;
    private Long shopId;
    private Long auditId;
    private Date auditTime = new Date();
    private String note;


    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getAuditId() {
        return auditId;
    }

    public void setAuditId(Long auditId) {
        this.auditId = auditId;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ShopAuditLog{" +
        ", state=" + state +
        ", shopId=" + shopId +
        ", auditId=" + auditId +
        ", auditTime=" + auditTime +
        ", note=" + note +
        "}";
    }
}
