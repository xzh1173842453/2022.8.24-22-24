package cn.itsource.org.domain;

import cn.itsource.basic.domain.BaseDomain;
import lombok.Data;

/**
 * @description: 实体类：员工
 * @author: Bug修复机
 * @date: 2022年08月15日 ： 9:36
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Data
public class Employee  extends BaseDomain {
    private String username;
    private String phone;
    private String email;
    private String salt;
    private String password;
    private Integer age;
    private Integer state = 1;
    private Long department_id;
    private Long logininfo_id;
    private Long shop_id;
}
