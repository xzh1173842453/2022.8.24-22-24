package cn.itsource.org.domain;

import cn.itsource.basic.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 实体类：部门
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:20
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Data
public class Department extends BaseDomain {
    //部门编号
    private String sn;
    //部门名称
    private String name;
    //部门状态：1-启用，0-禁用
    private Integer state;

    //部门经理的ID - 对应t_employee
    private Long manager_id;
    private Employee manager;

    //上级部门ID - 对应t_department
    private Long parent_id;
    private Department parent;

    //用来保存当前部门的子部门，属性名要于elementUi组件中的属性一致，不能改
    //部门树最后一级没有数据就不显示
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Department> children = new ArrayList<>();
}
