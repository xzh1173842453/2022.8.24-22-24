package cn.itsource.org.service.impl;

import cn.itsource.org.domain.ShopAuditLog;
import cn.itsource.org.service.IShopAuditLogService;
import cn.itsource.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 业务实现类：
 */
@Service
public class ShopAuditLogServiceImpl extends BaseServiceImpl<ShopAuditLog> implements IShopAuditLogService {

}
