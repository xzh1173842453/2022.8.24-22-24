package cn.itsource.org.service.impl;

import cn.itsource.basic.exception.BusinessException;
import cn.itsource.basic.service.impl.BaseServiceImpl;
import cn.itsource.basic.util.BaiduAuditUtils;
import cn.itsource.org.domain.Employee;
import cn.itsource.org.domain.Shop;
import cn.itsource.org.domain.ShopAuditLog;
import cn.itsource.org.dto.ShopDto;
import cn.itsource.org.mapper.EmployeeMapper;
import cn.itsource.org.mapper.ShopAuditLogMapper;
import cn.itsource.org.mapper.ShopMapper;
import cn.itsource.org.service.IShopService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

/**
 * 业务实现类：
 */
@Service
public class ShopServiceImpl extends BaseServiceImpl<Shop> implements IShopService {

    @Autowired
    private ShopMapper shopMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private ShopAuditLogMapper shopAuditLogMapper;
    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    @Transactional
    public void settlement(Shop shop) {
        //一：校验 1.空值校验(写一个) 2.格式通过正则表达式（不写） 3.校验是否入驻过
        // 3.确认密码校验（不写） - 前端已经校验了后端还有校验，前端js代码校验可能会被跳过
        //空值校验 - a.直接使用字符串的方法 b.使用apache提供的lang3包 c.使用其他工具类
        if(StringUtils.isEmpty(shop.getName())){
            throw  new BusinessException("店铺名称不能为空");
        }
        Shop dbShop = shopMapper.loadByNameAndAddress(shop);
        if(dbShop != null){//说明根据名称和地址找到了 - 这个店铺注册过了
            throw  new BusinessException("店铺已经入驻，请直接登录!如果忘记密码，请联系管理员!!!");
        }
        //百度AI智能审核
        Boolean boo1 = BaiduAuditUtils.TextCensor(shop.getName() + shop.getAddress());
        if(!boo1){
            throw  new BusinessException("店铺名称和地址不合法");
        }
        if(!StringUtils.isEmpty(shop.getLogo())){
            String url = "http://123.207.27.208"+shop.getLogo(); //常量
            Boolean boo2 = BaiduAuditUtils.ImgCensor(url);
            if(!boo2){
                throw  new BusinessException("logo违规");
            }
        }


        //二：添加管理员信息 - 添加了之后就可以获取自增ID
        Employee admin = shop.getAdmin();
        employeeMapper.save(admin);

        //三：将自增id设置到t_shop的admin_id
        shop.setAdminId(admin.getId());

        //四：添加Shop
        shopMapper.save(shop);

        //五：将shop_id更新到t_employee
        admin.setShop_id(shop.getId());
        employeeMapper.update(admin);
    }

    @Override
    @Transactional
    public void auditPass(ShopAuditLog shopAuditLog) throws MessagingException { //shopId note
        //1.修改状态：1-2
        Shop shop = shopMapper.loadById(shopAuditLog.getShopId());
        shop.setState(2);
        shopMapper.update(shop);

        //2.添加审核日志
        shopAuditLog.setState(2);
        shopAuditLog.setAuditId(350L);
        shopAuditLogMapper.save(shopAuditLog);

        //3.发送激活邮件
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setFrom("659769497@qq.com");
        //给管理员发送邮件
        Employee employee = employeeMapper.loadById(shop.getAdminId());
        helper.setTo(employee.getEmail());
        helper.setSubject("店铺激活成功!!!");
        helper.setText("<h1>店铺激活邮件!!!</h1><h3><a href='http://localhost:8080/shop/active/"+shopAuditLog.getShopId()
                +"'>点击该链接激活店铺</a></h3>",true);
        javaMailSender.send(message);
    }

    @Override
    @Transactional
    public void auditReject(ShopAuditLog shopAuditLog) throws MessagingException {
        //1.修改状态：1-4
        Shop shop = shopMapper.loadById(shopAuditLog.getShopId());
        shop.setState(4);
        shopMapper.update(shop);

        //2.添加审核日志
        shopAuditLog.setState(4);
        shopAuditLog.setAuditId(350L);
        shopAuditLogMapper.save(shopAuditLog);

        //3.发送激活邮件
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setFrom("659769497@qq.com");
        //给管理员发送邮件
        Employee employee = employeeMapper.loadById(shop.getAdminId());
        helper.setTo(employee.getEmail());
        helper.setSubject("店铺激活邮件!!!");
        helper.setText("<h1>店铺激活失败!!!</h1><h3><a href='http://localhost:8081/#/register'>点击该链接重新入驻</a></h3>",true);
        javaMailSender.send(message);
    }

    @Override
    public List<ShopDto> echarts() {
        return shopMapper.echarts();
    }
}
