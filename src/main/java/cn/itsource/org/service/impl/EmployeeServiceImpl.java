package cn.itsource.org.service.impl;

import cn.itsource.basic.service.impl.BaseServiceImpl;
import cn.itsource.org.domain.Employee;
import cn.itsource.org.service.IEmployeeService;
import org.springframework.stereotype.Service;

/**
 * @description: 业务实现类：部门
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:21
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
@Service //1.将当前类交给Spring管理 2.实例化对象的注解 //前提：扫描包路径
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements IEmployeeService {


}
