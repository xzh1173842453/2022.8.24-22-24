package cn.itsource.org.service;

import cn.itsource.org.domain.ShopAuditLog;
import cn.itsource.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tom
 * @since 2022-08-22
 */
public interface IShopAuditLogService extends IBaseService<ShopAuditLog> {

}
