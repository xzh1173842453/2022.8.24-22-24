package cn.itsource.org.service;

import cn.itsource.org.domain.Shop;
import cn.itsource.basic.service.IBaseService;
import cn.itsource.org.domain.ShopAuditLog;
import cn.itsource.org.dto.ShopDto;

import javax.mail.MessagingException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Tom
 * @since 2022-08-21
 */
public interface IShopService extends IBaseService<Shop> {

    void settlement(Shop shop);

    void auditPass(ShopAuditLog shopAuditLog) throws MessagingException;

    void auditReject(ShopAuditLog shopAuditLog) throws MessagingException;

    List<ShopDto> echarts();
}
