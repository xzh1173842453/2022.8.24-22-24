package cn.itsource.test;

import cn.itsource.basic.util.PageList;
import cn.itsource.org.domain.Department;
import cn.itsource.org.query.DepartmentQuery;
import cn.itsource.org.service.IDepartmentService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @description: 测试类：部门
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:22
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public class TestDepartment extends BaseTest {

    @Autowired
    private IDepartmentService departmentService;

    @Test
    public void test1() throws Exception{
        departmentService.loadAll();
    }

    @Test
    public void test2() throws Exception{
        DepartmentQuery query = new DepartmentQuery();
        //设置第2页
        query.setCurrentPage(2);
        //调用API查询
        PageList<Department> list = departmentService.queryPage(query);
        //只打印查询的列表数据
        list.getData().forEach(System.out::println);
    }

}
