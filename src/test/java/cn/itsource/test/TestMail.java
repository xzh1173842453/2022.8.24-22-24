package cn.itsource.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * @description: 测试邮箱发送
 * @author: Bug修复机
 * @date: 2022年08月22日 ： 14:32
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
public class TestMail extends BaseTest{

    @Autowired
    private JavaMailSender javaMailSender;

    @Test
    public void test() throws Exception{
        SimpleMailMessage message = new SimpleMailMessage();
        //发件人
        message.setFrom("659769497@qq.com");
        //收件人
        message.setTo("1251078337@qq.com");
        //主题
        message.setSubject("新冠病毒防御指南!!!");
        //内容
        message.setText("好好在家呆着!!!");
        javaMailSender.send(message);
    }

    @Test
    public void test2() throws Exception{
        //创建复杂邮件信息
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //创建helper，参数2表示可以上传附件
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        //设置发件人
        helper.setFrom("659769497@qq.com");
        //设置收件人
        helper.setTo("1251078337@qq.com");
        //设置主题
        helper.setSubject("店铺激活邮件");
        //设置内容
        helper.setText("<h1>你的店铺已经注册!!!</h1><img src='http://123.207.27.208/group1/M00/01/73/CgAIC2MDHpqAAR9_AABczSTDdNk492.jpg'><a href='http://localhost:8080/shop/active/22'>点击该链接激活</a>",true);
        //添加附件
        helper.addAttachment("p68.jpeg",new File("C:\\Users\\admin\\Pictures\\p68.jpeg"));
        //发送邮件
        javaMailSender.send(mimeMessage);
    }

}
