package cn.itsource.test;

import cn.itsource.App;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @description: 基础测试类
 * @author: Bug修复机
 * @date: 2022/8/12 : 19:22
 * @version: ver1.0
 * @email tangli@itsource.cn
 */
//开启Spring测试
@RunWith(SpringRunner.class)
//开启SpringBoot测试并加载自动配置
@SpringBootTest(classes = App.class)
public class BaseTest {
}
